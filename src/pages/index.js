export default [
    {
        path: '/',
        getPage: () => import('./home')
    },
    {
        path: '/about',
        getPage: () => import('./about')
    },
    {
        path: '/demo',
        getPage: () => import('./demo')
    }
]
