import React from 'react';
import { Page, usePage, usePageState } from '@weilluo/react-ssr'

function View() {
    const page = usePage()
    const state = usePageState()

    return (<>
        <div className='about' onClick={page.plusNumber}>
            About {state.number}
        </div>

        <Comp />
        <CompMemo />
    </>)
}

function Comp() {
    const state = usePageState()
    console.log('Comp aaa')
    return (
        <div>test comp {state.number}</div>
    )
}
const CompMemo = React.memo(Comp)

export default class Demo extends Page {
    View = View
    styles = []

    getInitialState = async () => {
        return {
            title: 'About',
            number: 1,
        }
    }

    plusNumber = () => {
        const state = this.store.getState()
        this.store.dispatch({ number: state.number + 1 })
    }
}
