import React from 'react';
import { Page, usePageState } from '@weilluo/react-ssr'

function View() {
    const state = usePageState()

    return (
        <div className='app'>
            <button onClick={() => console.info(123)}>
                {state.buttonName}123
            </button>
        </div>
    )
}

export default class Home extends Page {
    View = View
    styles = ['home']

    getInitialState = async () => {
        return {
            title: '读书',
            buttonName: '点击搜索图书',
        }
    }
}
