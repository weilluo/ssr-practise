import React from 'react';
import { Page } from '@weilluo/react-ssr'


// 1. Use React.memo

function View() {
    let [prop1, setProp1] = React.useState(0)
    let [prop2, setProp2] = React.useState(0)
    console.log('****************************')
    return (
        <div className='demo'>
            <p className='title'>React memo</p>

            <button onClick={() => setProp1(prop1 + 1)}>Prop1 ++</button>
            <button onClick={() => setProp2(prop2 + 1)}>Prop2 ++</button>

            <CompMemo prop1={prop1} />

            <Comp prop2={prop2} />
        </div>
    )
}

function Comp({ prop2 }) {
    console.log('Without React.meno')
    return (
        <div>prop2: {prop2}</div>
    )
}

function Comp1({ prop1 }) {
    console.log('React.meno 11111111111111')
    return (
        <div>prop1: {prop1}</div>
    )
}
const CompMemo = React.memo(Comp1)

export default class Demo extends Page {
    View = View
    styles = ['demo']

    getInitialState = async () => {
        return {
            title: 'Demo',
        }
    }
}
