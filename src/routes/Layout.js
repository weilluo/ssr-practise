import React from 'react'

export default function Layout(props) {
    const { initialState, assets, publicPath, styleContents } = props

    return (
        <html>
            <head>
                {styleContents?.map((styleContent, index) => 
                    <style key={index} type="text/css" dangerouslySetInnerHTML={{ __html: styleContent }}></style>
                )}
                <title>{initialState.title}</title>
            </head>
            <body>
                <div id="root" dangerouslySetInnerHTML={{ __html: props.bodyContent || '' }} />

                <script dangerouslySetInnerHTML={{ __html: props.scriptContent }}></script>

                <script src={`${publicPath}/js/${assets.index}`}></script>
                <script src={`${publicPath}/js/${assets.vendor}`}></script>
            </body>
        </html>
    )
}
