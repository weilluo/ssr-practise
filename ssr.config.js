const path = require('path')

module.exports = {
    port: 4001,
    basename: '/',
    publicPath: '',
    useLess: true,
    useSass: true,
    useHttp2: true,
    tslFiles: {
        key: path.resolve(__dirname, 'tsl', 'localhost-privkey.pem'),
        cert: path.resolve(__dirname, 'tsl', 'localhost-cert.pem'),
    },
}
